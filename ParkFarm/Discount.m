//
//  Discount.m
//  ParkFarm
//
//  Created by Jeremy Bassett on 23/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import "Discount.h"
#import "DiscountGroup.h"


@implementation Discount

@dynamic company;
@dynamic info;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic url;
@dynamic orderId;
@dynamic parentGroup;

@end
