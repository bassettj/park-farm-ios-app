//
//  AppDelegate+MOC.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 07/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit
import CoreData

extension AppDelegate {
    
    // Returns the URL to the application's Documents directory
    func applicationDocumentsDirectory() -> NSString {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        println("documentsPath: \(documentsPath)")
        return documentsPath
    }
    
    func saveContext(managedObjectContext : NSManagedObjectContext) {
        
    }
    /*
    func createMainQueueManagedObjectContext() -> NSManagedObjectContext {
        var managedObjectContext: NSManagedObjectContext
        var coordinator: NSPersistentStoreCoordinator? = self.createPersistentStoreCoordinator()
        /*
        if coordinator != nil {
        }
        */
        managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        println("Managed Object Context created successfully: \(managedObjectContext)")
        return managedObjectContext;
        
    }
    */
    /*
    - (NSManagedObjectContext *)createMainQueueManagedObjectContext
    {
    NSManagedObjectContext *managedObjectContext = nil;
    NSPersistentStoreCoordinator *coordinator = [self createPersistentStoreCoordinator];
    if (coordinator != nil) {
    managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
    }
    */
    /*
    // Returns the managed object model for the application.
    // If the model doesn't already exist, it is created from the application's model.
    - (NSManagedObjectModel *)createManagedObjectModel
    {
    NSManagedObjectModel *managedObjectModel = nil;
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Photomania" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
    }
    */
    /*
    func createManagedObjectModel() -> NSManagedObjectModel {
        let modelURL = NSBundle.mainBundle().URLForResource("DiscountModel", withExtension: "momd")
        println("Creating Managed Object Model with \(modelURL)")
        if let managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL!) {
            println("Managed Object Model created successfully")
            return managedObjectModel
        }
        else {
            println("Managed Object Model failed")
            return NSManagedObjectModel()
        }
    }
    
    // Returns the persistent store coordinator for the application.
    // If the coordinator doesn't already exist, it is created and the application's store added to it.
    func createPersistentStoreCoordinator() -> NSPersistentStoreCoordinator {
        var persistentStoreCoordinator: NSPersistentStoreCoordinator
        var managedObjectModel: NSManagedObjectModel = self.createManagedObjectModel()
        var docsDir = self.applicationDocumentsDirectory()
        var storeURL: NSURL = NSURL(fileURLWithPath: self.applicationDocumentsDirectory())!
        storeURL = storeURL.URLByAppendingPathComponent("Discounts.sqlite")
        println("Creating Persistent Store at \(storeURL)")
        var error: NSError?
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil, error: &error)
        if error != nil {
            println("Unresolved error: \(error), \(error?.userInfo)")
        }
        else {
            println("Persistent Store created successfully")
        }

        return persistentStoreCoordinator
    }
    */
    /*
    - (NSPersistentStoreCoordinator *)createPersistentStoreCoordinator
    {
    NSPersistentStoreCoordinator *persistentStoreCoordinator = nil;
    NSManagedObjectModel *managedObjectModel = [self createManagedObjectModel];
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MOC.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:managedObjectModel];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
    */
    /*
    Replace this implementation with code to handle the error appropriately.
    
    abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
    
    Typical reasons for an error here include:
    * The persistent store is not accessible;
    * The schema for the persistent store is incompatible with current managed object model.
    Check the error message to determine what the actual problem was.
    
    
    If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
    
    If you encounter schema incompatibility errors during development, you can reduce their frequency by:
    * Simply deleting the existing store:
    [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
    
    * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
    @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
    
    Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
    
    */
    /*
    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    abort();
    }

    return persistentStoreCoordinator;
    }
    
    */
    
    
    
}
