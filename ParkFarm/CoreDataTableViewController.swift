//
//  CoreDataTableTableViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 05/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit
import CoreData

class CoreDataTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var debug : Bool!
    
    // MARK: View Controller Lifecycle Methods
    override func awakeFromNib() {
        
        // grab the Core Data context and reload data if changes are made
        let context = DataAccess.sharedInstance.context
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("updateTableFollowingCoreDataChanges"), name: NSManagedObjectContextObjectsDidChangeNotification, object: context)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private lazy var fetchResultsController: NSFetchedResultsController? = {
        
        let context = DataAccess.sharedInstance.context
        
        let fetchRequest = NSFetchRequest(entityName: "Discount")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "parentGroup.name", ascending: true), NSSortDescriptor(key: "orderId", ascending: true)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: "parentGroup.name", cacheName: nil)
        do {
            try controller.performFetch()
            controller.delegate = self
            return controller
        } catch {
            return nil
        }
        //controller.performFetch()
        }()
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numRows = 0
        if let info = fetchResultsController!.sections![section] as? NSFetchedResultsSectionInfo {
            numRows = info.numberOfObjects
        }
        return numRows
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionHeaderText = ""
        if let sections = fetchResultsController!.sections {
            sectionHeaderText = sections[section].name
            return sectionHeaderText
        }
        else {
            print("titleForHeaderInSection: Sections Object not found")
            return ""
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var numSections = 1
        if let sections = fetchResultsController!.sections {
            numSections = sections.count;
        }
        return numSections
    }
    
    
}
