//
//  Gallery.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 06/04/2016.
//  Copyright © 2016 Jeremy Bassett. All rights reserved.
//

import Foundation

enum PageType: Int {
    case Conferences, Weddings, Occasions
}

class Gallery {
    
    var pageType: PageType
    
    init(pageType: PageType) {
        self.pageType = pageType
    }
    
    func images() -> [String] {
        switch pageType {
        case .Conferences:
            return ["Conference-Image-1", "Conference-Image-2", "Conference-Image-3"]
        case .Weddings:
            return ["Weddings-Image-1", "Weddings-Image-2", "Weddings-Image-3"]
        case .Occasions:
            return ["Occasions-Image-1"]
        }
    }
    
    func pageTitle() -> String {
        switch pageType {
        case .Conferences:
            return "Conferences"
        case .Weddings:
            return "Weddings"
        case .Occasions:
            return "Occasions"
        }
    }
    
    func pageText() -> String {
        switch pageType {
        case .Conferences:
            return "Whatever your business, your corporate events are taken seriously at Park Farm Hotel.\n\nYour delegates‘ needs will be catered for promptly and efficiently by experienced, enthusiastic staff.\n\nFrom 120 people capacity, seminars to private meetings of 2 people, we have conference suites to suite all needs and requirements. Hot and cold refreshments, be it a platter of sandwiches to a three course gourmet dinner can be requested for your attendees. Here at Park Farm Hotel we can provide the facilities and service specific to your needs as a large scale company or a private business.\n\nEquipment is available for your perusal such as projectors and large wall mounted screens, as well as photocopying and faxing services. You can be assured of an efficient, friendly team on hand to assist you at any time.\n"
        case .Weddings:
            return "If you are looking for a venue to hold your dream wedding then look no further than Park Farm Hotel Norwich. Set in picturesque gardens and Norfolk countryside, it is an idealistic setting to celebrate your special day with your family and friends. From that moment you arrive with your loved one to enquire, until the moment you leave as husband and wife, you can be assured of the best quality service to give you peace of mind on your big day.\n\nFrom a small intimate affair, to the full works of a big white wedding, our ever popular wedding co-ordinator Sue Dedman is on hand to make sure your wedding day will create memories that will be with you for a lifetime."
        case .Occasions:
            return "Park Farm Hotel is the ideal location for Anniversaries, Birthday Parties, Christenings and any other kind of celebration.\n\n            We will give you our undivided attention, and make it our business to ensure your event is an occasion to remember."
        }
    }
    
}
