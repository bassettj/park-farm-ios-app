//
//  PFCoreDataAccess+Seeding.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 09/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

extension PFCoreDataAccess {
    
    func printAllDiscountGroups() {
        let discFetchRequest = NSFetchRequest(entityName: "DiscountGroup")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        //let allDiscountGroups = context!.executeFetchRequest(discFetchRequest, error: nil) as! [DiscountGroup]
        
        let allDiscountGroups: [DiscountGroup]
        do {
            allDiscountGroups = try context.executeFetchRequest(discFetchRequest) as! [DiscountGroup]
            for discGroup in allDiscountGroups {
                print("Discount Group: \(discGroup.name) \n-------\n")
            }
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
    }
    /*
    override func printAllDiscounts() {
        let discFetchRequest = NSFetchRequest(entityName: "Discount")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let allDiscounts = context.executeFetchRequest(discFetchRequest, error: nil) as [Discount]
        
        for discount in allDiscounts {
            print("Discount Title: \(discount.title) Discount Subtitle: \(discount.subtitle) \n-------\n")
        }
    }
    */
    func seedDiscounts() {
        
        // Grab Discounts
        let discGroupFetchRequest = NSFetchRequest(entityName: "Discount")
        let allDiscountGroups: [DiscountGroup]
        do {
            allDiscountGroups = try context.executeFetchRequest(discGroupFetchRequest) as! [DiscountGroup]
            for discGroup in allDiscountGroups {
                print("Discount Group: \(discGroup.name) \n-------\n")
            }
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        // Create an array of "animal" tuples, assigning
        // whatever Classification and Zoo make sense
        let discounts = [
            (title: "Disc 1", subtitle: "Excellent Discount 1", url: "www.staylocal.co.uk", group: "Main Discounts"),
            (title: "Disc 2", subtitle: "Excellent Discount 2", url: "www.staylocal.co.uk", group: "Main Discounts"),
            (title: "Disc 3", subtitle: "Excellent Discount 3", url: "www.staylocal.co.uk", group: "Special Discounts"),
            (title: "Disc 4", subtitle: "Excellent Discount 4", url: "www.staylocal.co.uk", group: "Special Discounts"),
        ]
        /*
        // Create -actual- Animal instances and insert them
        for discount in discounts {
            let newDiscount = NSEntityDescription.insertNewObjectForEntityForName("Discount", incontext: context!) as Discount
            newDiscount.title = discount.title
            newDiscount.subtitle = discount.subtitle
            newDiscount.url = discount.url
        }
        */
        do {
            try context.save()
        } catch {}
        //context.save()
    }
    
    func seedDiscountGroups() {
        let discountGroups = [
            (title: "Main Discounts", subtitle: ""),
            (title: "Special Discounts", subtitle: "")
        ]
        
        for discGroup in discountGroups {
            /*
            let newDiscountGroup = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", incontext: context!) as DiscountGroup
            newDiscountGroup.groupName = discGroup.title
            */
        }
        
        do {
            try context.save()
        } catch {}
    }
    
}


