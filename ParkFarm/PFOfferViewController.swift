//
//  PFOfferViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 28/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit
import MapKit

class PFOfferViewController: UIViewController, UIActionSheetDelegate {

    @IBOutlet var headline: UILabel!
    @IBOutlet var subheader: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var descriptionTextView: UITextView!
    
    var discount: Discount?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headline.text = discount?.name
        subheader.text = discount?.info
        descriptionTextView.text = discount?.url
        //print("Discount: \(discount.show())")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: Selector("actionSelected:"))
    }
    
    func actionSelected(sender: UIBarButtonItem) {
        let sheet = UIActionSheet(title: "Actions", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Add Offer to Passbook", "View Offer on Website")
         sheet.showInView(view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        self.title = discount?.info
        super.viewDidAppear(animated)
        updateEmbeddedMap()
    }
    
    // MARK: - Navigation
    var smvc: SimpleMapViewController?
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("Preparing for Segue: \(segue.identifier)")
        if segue.identifier == "Simple Map View" {
            smvc = segue.destinationViewController as? SimpleMapViewController
        }
        else if segue.identifier == "Display Offer" {
            if let destinationViewController = segue.destinationViewController as? PFWebViewController {
                print("Web View Found")
                destinationViewController.websiteAddress = discount?.url
                destinationViewController.title = discount?.name
            }
        }
    }
    
    func updateEmbeddedMap() {
        print("Updating Embedded Map")
        if let mapView = smvc?.mapView {
            print("Found Map View")
            /*
            if let annotation = discount?.annotationForOffer() {
                mapView.removeAnnotations(mapView.annotations)
                mapView.addAnnotation(annotation)
                mapView.showAnnotations(mapView.annotations, animated: true)
            }
 */
        }
        else {
            print("ERROR: Map View not found")
        }
    }

}
