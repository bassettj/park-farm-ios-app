//
//  DiscountGroup+Create.swift
//  FetchResultsViewController
//
//  Created by Jeremy Bassett on 14/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

extension DiscountGroup {
    
    var orderedGroupName: String {
        return "/(orderId)" + " " + name
    }
    
    class func CreateDiscountGroupsFromArray(array discGroupArray: NSArray, inManagedObjectContext context: NSManagedObjectContext) {
        
        //let context = DataAccess.sharedInstance.context
        let debug = DataAccess.sharedInstance.debug
        
        for bundleItem in discGroupArray {
            let newDiscountGroup = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", inManagedObjectContext: context) as! DiscountGroup
            if let sectionString: AnyObject? = bundleItem["section"] {
                var section = sectionString! as! String
                if debug == true {
                    print("Section Title = \(section)")
                }
                newDiscountGroup.name = section
                if let orderId = bundleItem["orderId"]! as? NSNumber {
                    //newDiscountGroup.orderId = orderId
                }
            }
            let discountSet = NSMutableSet()
            if let rowArr: AnyObject? = bundleItem["rows"] {
                var rows = rowArr! as! NSArray
                if debug == true {
                    print("Row Array: \(rows)")
                }
                for rowItem in rows {
                    let newDiscount = NSEntityDescription.insertNewObjectForEntityForName("Discount", inManagedObjectContext: context) as! Discount
                    if debug == true {
                        print("Row Item = \(rowItem)")
                    }
                    if let title = rowItem["title"]! as? NSString {
                        newDiscount.name = title as String
                    }
                    if let latitude = rowItem["latitude"]! as? NSString {
                        newDiscount.latitude = NSNumber(double: latitude.doubleValue)
                    }
                    if let longitude = rowItem["longitude"]! as? NSString {
                        newDiscount.longitude = NSNumber(double: longitude.doubleValue)
                    }
                    if let subtitle = rowItem["subtitle"]! as? NSString {
                        newDiscount.info = subtitle as String
                    }
                    if let url = rowItem["url"]! as? NSString {
                        newDiscount.url = url as String
                    }
                    if let orderId = rowItem["orderId"]! as? NSString {
                        newDiscount.orderId = orderId.integerValue
                    }
                    discountSet.addObject(newDiscount)
                }
            }
            if debug == true {
                print("Discount Set = \(discountSet)")
            }
            newDiscountGroup.addDiscounts(discountSet as Set<NSObject>)

            do {
                try context.save()
            } catch {
                // Handle Error
            }

            //context.save(nil)
        }
    }
    
    class func DiscountGroupWithDictionary(dictionary dict: NSDictionary, inManagedObjectContext context: NSManagedObjectContext) -> DiscountGroup {
        
        var newDiscountGroup: DiscountGroup = DiscountGroup()
        if let sectionString: AnyObject? = dict["section"] {
            newDiscountGroup.name = sectionString! as! String
            
            var rowSet = NSSet()
            if let rowArray: AnyObject? = dict["rows"] as? NSArray {
                var rowSet = Discount.DiscountSetFromArray(rowArray! as! NSArray, inManagedObjectContext: context)
            }
            newDiscountGroup.discounts = rowSet as! Set<Discount>
            
        }
        return newDiscountGroup

    }

//    class func DiscountGroupWith(name groupName: NSString, inManagedObjectContext context: NSManagedObjectContext) -> DiscountGroup {
//        
//        var discountGroup: DiscountGroup = DiscountGroup()
//        
//        if groupName.length > 0 {
//            let fetchRequest = NSFetchRequest(entityName: "DiscountGroup")
//            fetchRequest.predicate = nil;
//            
//            var matched: [DiscountGroup] = []
//            
//            do {
//                matched = try context.executeFetchRequest(fetchRequest) as! [DiscountGroup]
//            } catch let error as NSError {
//                print("Error: \(error.localizedDescription)")
//                // TODO
//                return matched.first!
//            }
//            
//            //discountGroup = matchArray.first!
//
//            //let matches = context.executeFetchRequest(fetchRequest, error: nil)
//            /*
//            if let matchArray = matched {
//                if matchArray.count == 0 {
//                    // handle error
//                } else {
//                    discountGroup = matchArray.first!
//                }
//            }
//            else {
//                // handle error
//            }
//*/
//        }
//        else {
//            return nil
//        }
//        //return matchArray.first!
//    }
    
}
