//
//  PFDataAccess.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 09/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFDataAccess {
   
    var context: NSManagedObjectContext!
    
    init() {
        self.context = NSManagedObjectContext()
    }
    /*
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    */
    func printAllDiscountGroups() {
        let discFetchRequest = NSFetchRequest(entityName: "DiscountGroup")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let allDiscountGroups: [DiscountGroup]
        do {
            allDiscountGroups = try context.executeFetchRequest(discFetchRequest) as! [DiscountGroup]
            for discGroup in allDiscountGroups {
                print("Discount Group: \(discGroup.name) \n-------\n")
            }
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        //let allDiscountGroups = context.executeFetchRequest(discFetchRequest, error: nil) as! [DiscountGroup]
        
    }
    
    func printAllDiscounts() {
        let discFetchRequest = NSFetchRequest(entityName: "Discount")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let allDiscounts: [Discount]
        do {
            allDiscounts = try context.executeFetchRequest(discFetchRequest) as! [Discount]
            for discount in allDiscounts {
                print("Discount Title: \(discount.name) Discount Subtitle: \(discount.info) \n-------\n")
            }
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        //let allDiscounts = context.executeFetchRequest(discFetchRequest, error: nil) as! [Discount]
        
    }
    
    func seedDiscounts() {
        
        // Grab Discounts
        let discGroupFetchRequest = NSFetchRequest(entityName: "Discount")
        //let alldiscGroups = context.executeFetchRequest(discGroupFetchRequest, error: nil) as! [Discount]
        
        let alldiscGroups: [Discount]
        do {
            alldiscGroups = try context.executeFetchRequest(discGroupFetchRequest) as! [Discount]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        // Create an array of "animal" tuples, assigning
        // whatever Classification and Zoo make sense
        let discounts = [
            (title: "Disc 1", subtitle: "Excellent Discount 1", url: "www.staylocal.co.uk", group: "Main Discounts"),
            (title: "Disc 2", subtitle: "Excellent Discount 2", url: "www.staylocal.co.uk", group: "Main Discounts"),
            (title: "Disc 3", subtitle: "Excellent Discount 3", url: "www.staylocal.co.uk", group: "Special Discounts"),
            (title: "Disc 4", subtitle: "Excellent Discount 4", url: "www.staylocal.co.uk", group: "Special Discounts"),
        ]
        
        // Create -actual- instances and insert them
        for discount in discounts {
            let newDiscount = NSEntityDescription.insertNewObjectForEntityForName("Discount", inManagedObjectContext: context) as! Discount
            newDiscount.name = discount.title
            newDiscount.info = discount.subtitle
            newDiscount.url = discount.url
        }
        
        do {
            try context.save()
        } catch {
            
        }
        //context.save(nil)
    }
    
    func seedDiscountGroups() {
        let discountGroups = [
            (title: "Main Discounts", subtitle: ""),
            (title: "Special Discounts", subtitle: "")
        ]
        
        for discGroup in discountGroups {
            let newDiscountGroup = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", inManagedObjectContext: context) as! DiscountGroup
            newDiscountGroup.name = discGroup.title
        }
        
        do {
            try context.save()
        } catch {
            
        }
    }
    
    
    
    
}
