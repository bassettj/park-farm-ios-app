//
//  PFMasterViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 04/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit
import MessageUI

class PFMasterViewController: UITableViewController, UIAlertViewDelegate, MFMailComposeViewControllerDelegate {
    
    var menus = [PFMenuItem]?()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menus = [PFMenuItem]()
        menus?.append(PFMenuItem(pagetitle: "Breakfast Menu", pdfFile: "Bar-Lunch-Menu.pdf", pdfURL: "\(WebServiceInfo.PdfLocation))/breakfast-menu.pdf"))
        menus?.append(PFMenuItem(pagetitle: "Lunch Menu", pdfFile: "Bar-Lunch-Menu.pdf", pdfURL: ""))
        menus?.append(PFMenuItem(pagetitle: "Room Service", pdfFile: "Bar-Lunch-Menu.pdf", pdfURL: ""))
        
        print(menus)
        
    }
    
    func prepareMailMessage() {
        print("Preparing E-Mail...")
        if MFMailComposeViewController.canSendMail() == false {
            let alert = UIAlertView(title: "Sorry", message: "No mail accounts found!", delegate: self, cancelButtonTitle: "Dismiss")
            alert.show()
        }
        else {
            let mailController: MFMailComposeViewController = MFMailComposeViewController()
            mailController.mailComposeDelegate = self
            mailController.setSubject("")
            mailController.setToRecipients([""])
            mailController.setMessageBody("", isHTML: false)
            self.presentViewController(mailController, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error:NSError?) {
        
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            NSLog("Mail cancelled")
        case MFMailComposeResultSaved.rawValue:
            NSLog("Mail saved")
        case MFMailComposeResultSent.rawValue:
            NSLog("Mail sent")
        case MFMailComposeResultFailed.rawValue:
            NSLog("Mail sent failure: %@", [error!.localizedDescription])
        default:
            break
        }
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    func preparePhoneCall() {
        print("Preparing Phonecall...")
        let telURL = NSURL(string: "tel://+441603810264")
        if UIApplication.sharedApplication().canOpenURL(telURL!) {
            UIApplication.sharedApplication().openURL(telURL!)
        }
        else {
            let alert = UIAlertView(title: "Sorry", message: "This device cannot make phonecalls", delegate: self, cancelButtonTitle: "Dismiss")
            alert.show()
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 && indexPath.row == 2 {
            prepareMailMessage()
        }
        else if indexPath.section == 0 && indexPath.row == 1 {
            preparePhoneCall()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            print("Identifier: \(identifier)")
            if let destinationViewController = segue.destinationViewController as? PFWebViewController {
                switch identifier {
                case SegueNames.ShowWeather:
                    print("segue = \(SegueNames.ShowWeather)")
                    destinationViewController.websiteAddress = "http://www.bbc.co.uk/weather/2641181"
                    destinationViewController.title = "Local Weather"
                case SegueNames.ShowClimate:
                    print("segue = \(SegueNames.ShowClimate)")
                    destinationViewController.websiteAddress = "http://en.wikipedia.org/wiki/Norwich#Climate"
                    destinationViewController.title = "Local Climate"
                case SegueNames.HotelWebsite:
                    print("segue = \(SegueNames.HotelWebsite)")
                    destinationViewController.websiteAddress = "http://www.parkfarm-hotel.co.uk"
                    destinationViewController.title = "Park Farm Website"
                default: break
                }
            }
            else if let destinationViewController = segue.destinationViewController as? PFDiscountsViewController {
                switch identifier {
                case SegueNames.ShowDiscounts:
                    destinationViewController.title = "Local Discounts"
                default: break
                }
            }
            else if let destinationViewController = segue.destinationViewController as? PFConciergeViewController {
                switch identifier {
                case SegueNames.ShowDiscounts:
                    destinationViewController.title = "Concierge Services"
                    //destinationViewController.roomService = PFRoomService(seeding: true)
                default: break
                }
            }
            else if let destinationViewController = segue.destinationViewController as? JBPDFViewController {
                switch identifier {
                case SegueNames.ShowBreakfastMenu:
                    if let indexPath = tableView.indexPathForSelectedRow {
                        let menuItem = menus![indexPath.row] as PFMenuItem
                        print("menuItem = \(menuItem)")
                        destinationViewController.menu = menuItem
                    }
                case SegueNames.ShowLunchMenu:
                    if let indexPath = tableView.indexPathForSelectedRow {
                        let menuItem = menus![indexPath.row] as PFMenuItem
                        print("menuItem = \(menuItem)")
                        destinationViewController.menu = menuItem
                    }
                case SegueNames.ShowRoomServiceMenu:
                    if let indexPath = tableView.indexPathForSelectedRow {
                        let menuItem = menus![indexPath.row] as PFMenuItem
                        print("menuItem = \(menuItem)")
                        destinationViewController.menu = menuItem
                    }
                default: break
                }
            }
        }
    }

}
