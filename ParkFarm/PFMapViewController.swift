//
//  PFMapViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 03/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import MapKit

class PFMapViewController : UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    //var annotations: [OfferAnnotation]?
    var locationFound = false
    
    @IBOutlet var mapView: MKMapView! {
        didSet {
            //updateUI()
        }
    }
    
    let locationManager = CLLocationManager()
    var location: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = 30.0
        locationManager.distanceFilter = 10.0
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapView.showsUserLocation = true
        mapView.delegate = self
        
        let hotel = OfferAnnotation(coordinate: CLLocationCoordinate2DMake(52.588727, 1.169051), title: "Park Farm Hotel", subtitle: "Exclusive Country House Hotel")
        mapView.addAnnotation(hotel)
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        //locationFound = true
    }
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if let anno = annotation as? MKUserLocation {
            return nil
        }

        var view = mapView.dequeueReusableAnnotationViewWithIdentifier("Map")
        if view == nil {
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Map")
            // TODO
            view!.canShowCallout = false
        }
        view!.annotation = annotation
        return view
    }
    
    func showRoute(response: MKDirectionsResponse) {
        
        for route in response.routes as! [MKRoute] {
            for step in route.steps {
                print(step.instructions)
            }
            self.mapView.addOverlay(route.polyline,
                level: MKOverlayLevel.AboveRoads)
        }
    }

    
    @IBAction func getDirections(sender: UIBarButtonItem) {
        let request = MKDirectionsRequest()
        request.source = MKMapItem.mapItemForCurrentLocation()
        
        //request.setSource(MKMapItem.mapItemForCurrentLocation())
        
        let destination: MKMapItem = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2DMake(52.588727, 1.169051), addressDictionary: nil))
        
        request.destination = destination
        //request.setDestination(destination)
        request.requestsAlternateRoutes = false
        /* TODO
        let directions = MKDirections(request: request)
        
        directions.calculateDirectionsWithCompletionHandler({(response:
            MKDirectionsResponse!, error: NSError!) in
            
            if error != nil {
                print("Directions Error")
                // Handle error
            } else {
                self.showRoute(response)
            }
        })
        */
    }
    
    func mapView(mapView: MKMapView!, rendererForOverlay
        overlay: MKOverlay!) -> MKOverlayRenderer! {
            let renderer = MKPolylineRenderer(overlay: overlay)
            
            renderer.strokeColor = UIColor.blueColor()
            renderer.lineWidth = 5.0
            return renderer
    }

    func updateUI() {
        
    }
    /* TODO
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        print("updating")
        if let readings = locations as? [CLLocation] {
            location = readings.last
            print("\(location?.horizontalAccuracy)")
            if location != nil {
                if locationFound == false {
                    mapView.showAnnotations(mapView.annotations, animated: true)
                    if (location?.horizontalAccuracy.isZero != nil) {
                        //locationFound = true
                    }
                    
                }
                //mapView.setCenterCoordinate(CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude), animated: true)
            }
        }
    }
    */
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        print(error.description)
    }
}
