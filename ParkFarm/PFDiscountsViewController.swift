//
//  PFDiscountsViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 04/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFDiscountsViewController: UITableViewController {

    var discounts = [["Discount 1", "Discount 2"], ["Discount 3", "Discount 4"]]
    var discountTitles = ["Main", "Other"]
    //var discounts = [String : <String>Array]()
    
    //var discounts = Dictionary<String, Array<String>>();
    //var l : Array = ["fdfsd", "fsdfsd"]
    //discounts.addValue ["Other Discounts"] = l
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table View Delegate Methods
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return discountTitles.count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return discountTitles[section];
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //keys = discounts.allKeys
        return discounts[section].count
    }
    
    private struct Storyboard {
        static let cellReuseIdentifier = "Discount Cell"
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Storyboard.cellReuseIdentifier, forIndexPath: indexPath) 
        cell.textLabel?.text = discounts[indexPath.section][indexPath.row]
        return cell
    }
    

    // MARK: - Navigation
    /*
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destViewController = segue.destinationViewController as? PFOfferViewController {
            if let indexPath = tableView.indexPathForSelectedRow() {
                let discount = discounts[indexPath.section][indexPath.row] as Discount
                destViewController.discount = discount
            }
        }
    }
    */
}
