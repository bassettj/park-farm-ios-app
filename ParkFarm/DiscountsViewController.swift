//
//  DiscountsViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 08/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class DiscountsViewController: CoreDataTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Loading DiscountsViewController....")
    }
    
    func updateTableFollowingCoreDataChanges() {
        print("Realoading Data following Core Data changes...")
        do {
            try fetchResultsController.performFetch()
                tableView.reloadData()
        } catch {}
        //fetchResultsController.performFetch()
    }
    
    private lazy var fetchResultsController: NSFetchedResultsController = {
        
        let context = DataAccess.sharedInstance.context
        
        let fetchRequest = NSFetchRequest(entityName: "Discount")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "parentGroup.name", ascending: true), NSSortDescriptor(key: "name", ascending: true)]
        
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: "parentGroup.name", cacheName: nil)
        
        do {
            try controller.performFetch()
            controller.delegate = self
        } catch {}
//        controller.performFetch()
        
        return controller
        }()
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print("Awaking from NIB (DiscountsViewController)")
    }
    
    // MARK: Table View Controller Data Source Methods
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var sectionHeaderText = ""
        if let sections = fetchResultsController.sections {
            sectionHeaderText = sections[section].name
            let plainSectionHeaderText = stringByRemovingOrdering(sectionHeaderText)
            return plainSectionHeaderText
        }
        else {
            print("titleForHeaderInSection: Sections Object not found")
            return ""
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Discount Cell", forIndexPath: indexPath) as! UITableViewCell
        
        var discount: Discount = fetchResultsController.objectAtIndexPath(indexPath) as! Discount
        
        cell.textLabel?.text = discount.name
        cell.detailTextLabel?.text = discount.info
        return cell
    }

    // MARK: Table View Controller Delegate Methods
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    /*
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let cell = sender! as? UITableViewCell {
            let indexPath = self.tableView.indexPathForCell(cell)
            prepareForSegue(segue, sender: indexPath)
        }
    }
    */
    /*
    func prepareViewController(vc: AnyObject, forSegue: String, indexPath: NSIndexPath) {
        if let discount = fetchResultsController.objectAtIndexPath(indexPath) as? Discount {
            if let viewController = vc as? UIViewController {
                //viewController.discount = discount;
            }
        }
    }
*/
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destViewController = segue.destinationViewController as? PFOfferViewController {
            if let indexPath = tableView.indexPathForSelectedRow {
                let discount = fetchResultsController.objectAtIndexPath(indexPath) as! Discount
                destViewController.discount = discount
            }
        }
    }

    
    // MARK: Supporting Convenience Methods
    func stringByRemovingOrdering(sectionString: String) -> String {
        if let orderTextRange = sectionString.rangeOfString("^\\[\\d+\\]\\s", options: .RegularExpressionSearch) {
            let plainSectionString = sectionString.stringByReplacingCharactersInRange(orderTextRange, withString: "")
            return plainSectionString
        }
        else {
            return sectionString
        }
    }

}
