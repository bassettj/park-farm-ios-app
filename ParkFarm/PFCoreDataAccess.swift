//
//  PFCoreDataAccess.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 09/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFCoreDataAccess {
    
    var context: NSManagedObjectContext!
    /*
        lazy var managedObjectContext: NSManagedObjectContext = {
        self.createMainQueueManagedObjectContext()
    } ()
    */
    // MARK: Singleton Methods
    class var sharedInstance : PFCoreDataAccess {
        struct Singleton {
            static let instance = PFCoreDataAccess()
        }
        return Singleton.instance
    }
    
    // MARK: Core Data Access Methods
    func createMainQueueManagedObjectContext() -> NSManagedObjectContext {
        var managedObjectContext = NSManagedObjectContext()
        var coordinator: NSPersistentStoreCoordinator? = self.createPersistentStoreCoordinator()
        managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        print("Managed Object Context created successfully: \(managedObjectContext)")
        return managedObjectContext;
        
    }
    
    // Returns the URL to the application's Documents directory
    func applicationDocumentsDirectory() -> NSString {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
        print("documentsPath: \(documentsPath)")
        return documentsPath
    }
    
    func createManagedObjectModel() -> NSManagedObjectModel {
        let modelURL = NSBundle.mainBundle().URLForResource("DiscountModel", withExtension: "momd")
        print("Creating Managed Object Model with \(modelURL)")
        if let managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL!) {
            print("Managed Object Model created successfully")
            return managedObjectModel
        }
        else {
            print("Managed Object Model failed")
            return NSManagedObjectModel()
        }
    }
    
    // Returns the persistent store coordinator for the application.
    // If the coordinator doesn't already exist, it is created and the application's store added to it.
    func createPersistentStoreCoordinator() -> NSPersistentStoreCoordinator {
        var persistentStoreCoordinator: NSPersistentStoreCoordinator
        var managedObjectModel: NSManagedObjectModel = createManagedObjectModel()
        var docsDir = self.applicationDocumentsDirectory()
        var storeURL: NSURL = NSURL(fileURLWithPath: self.applicationDocumentsDirectory() as String)
        storeURL = storeURL.URLByAppendingPathComponent("Discounts.sqlite")
        print("Creating Persistent Store at \(storeURL)")
        //var error: NSError?
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            try persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
            print("Persistent Store created successfully at \(storeURL)")
        } catch let error as NSError{
            print("Unresolved error: \(error), \(error.userInfo)")
        }
        return persistentStoreCoordinator
    }
    
    
    // MARK: Saving Methods
    func saveContext(managedObjectContext : NSManagedObjectContext) {
        
    }
    // MARK: Seeding Methods
    func printAllDiscounts() {
        let discFetchRequest = NSFetchRequest(entityName: "Discount")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let allDiscounts: [Discount]
        do {
            allDiscounts = try context.executeFetchRequest(discFetchRequest) as! [Discount]
            for discount in allDiscounts {
                print("Discount Title: \(discount.name) Discount Subtitle: \(discount.info) \n-------\n")
            }
        } catch let error as NSError {
            
        }
        
        //let allDiscounts = context.executeFetchRequest(discFetchRequest, error: nil) as! [Discount]
        
    }
    
}
