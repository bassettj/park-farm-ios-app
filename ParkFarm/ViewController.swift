//
//  ViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 03/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Main View Loading...")
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.popToRootViewControllerAnimated(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

