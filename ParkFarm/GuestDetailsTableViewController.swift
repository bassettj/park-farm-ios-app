//
//  GuestDetailsTableViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 19/04/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class GuestDetailsTableViewController: UITableViewController {

    var roomService: PFRoomService!
    @IBOutlet var surnameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var telephoneTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Stay Details"
        surnameTextField.text = roomService.guestSurname
        emailTextField.text = roomService.guestEMail
        telephoneTextField.text = roomService.guestPhoneNumber
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        roomService.guestSurname = surnameTextField.text
        roomService.guestEMail = emailTextField.text
        roomService.guestPhoneNumber = telephoneTextField.text
    }

}
