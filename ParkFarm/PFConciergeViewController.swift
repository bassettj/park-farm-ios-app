//
//  PFConciergeViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 01/04/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFConciergeViewController: UITableViewController, UIActionSheetDelegate {
    
    var roomService: PFRoomService!
    
    override func viewDidLoad() {
        roomService = PFRoomService(seeding: true)
        self.title = "Concierge"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: Selector("orderRequested:"))
        super.viewDidLoad()
    }
    
    func orderRequested(sender: UIBarButtonItem) {
        let actionSheet = UIActionSheet(title: "Concierge", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Place Order")
        actionSheet.showInView(view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            let url = roomService.apiCall()
            print("URL: \(url)");
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Service Cell", forIndexPath: indexPath) 
        
        cell.textLabel?.text = roomService.cellTitleForIndexPath(indexPath)
        cell.detailTextLabel?.text = roomService.cellSubtitleTitleForIndexPath(indexPath)
        
        if indexPath.section == 0 {
            cell.accessoryType = .DisclosureIndicator
        }
        else {
            if roomService.itemIsSelectedForIndexPath(indexPath) == true {
                cell.accessoryType = .Checkmark;
            }
            else {
                cell.accessoryType = .None;
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == roomService.numberOfSections() - 1 {
            return "\(roomService.orderValue())"
        }
        else {
            return ""
        }
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        roomService.markItemForIndexPath(indexPath, selected: !roomService.itemIsSelectedForIndexPath(indexPath))
        print("Order value: \(roomService.orderValue())")
        tableView.reloadData()
    }
        
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return roomService.sectionTitleForIndexPath(section)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return roomService.numberOfSections()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomService.numberOfCellForSection(section)
    }

}
