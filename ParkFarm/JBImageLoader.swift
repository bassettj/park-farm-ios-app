//
//  JBImageLoader.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 25/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//  Usage: 

//  JBImageLoader.sharedLoader.imageForUrl(urlString, completionHandler:{(image: UIImage?, url: String) in
//      self.imageView.image = image
//  })

import UIKit
import Foundation

class JBImageLoader {
    
    var cache = NSCache()
    
    class var sharedLoader : JBImageLoader {
        struct Static {
            static let instance : JBImageLoader = JBImageLoader()
        }
        return Static.instance
    }
    
    func imageForUrl(urlString: String, completionHandler:(image: UIImage?, url: String) -> ()) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {()in
            let data: NSData? = self.cache.objectForKey(urlString) as? NSData
            
            if let goodData = data {
                let image = UIImage(data: goodData)
                dispatch_async(dispatch_get_main_queue(), {() in
                    completionHandler(image: image, url: urlString)
                })
                return
            }
            
            let urlForRequest = NSURL(string: urlString)
            let request = NSURLRequest(URL: urlForRequest!)
            let downloadTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    completionHandler(image: nil, url: urlString)
                    return
                }
                
                if data != nil {
                    let image = UIImage(data: data!)
                    self.cache.setObject(data!, forKey: urlString)
                    dispatch_async(dispatch_get_main_queue(), {() in
                        completionHandler(image: image, url: urlString)
                    })
                    return
                }
            })
            downloadTask.resume()
        })
        
    }
}