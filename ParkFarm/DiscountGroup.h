//
//  DiscountGroup.h
//  ParkFarm
//
//  Created by Jeremy Bassett on 23/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Discount;

@interface DiscountGroup : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *discounts;
@end

@interface DiscountGroup (CoreDataGeneratedAccessors)

- (void)addDiscountsObject:(Discount *)value;
- (void)removeDiscountsObject:(Discount *)value;
- (void)addDiscounts:(NSSet *)values;
- (void)removeDiscounts:(NSSet *)values;

@end
