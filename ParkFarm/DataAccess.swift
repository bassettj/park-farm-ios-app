//
//  DataAccess.swift
//  FetchResultsViewController
//
//  Created by Jeremy Bassett on 12/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

class DataAccess {
    
    let debug = false
    
    class var sharedInstance : DataAccess {
        struct Singleton {
            static let instance = DataAccess()
        }
        return Singleton.instance
    }
    
    init() {
        print("Data Class Initialised...")
        
        //removeAllDiscountGroups()
        //removeAllDiscounts()
        
        NSNotificationCenter.defaultCenter().addObserverForName(Notification.WebServiceUpdatedNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (note) -> Void in
            if self.debug == true {
                print("Notification Received: \(note.userInfo)")
            }
            if let info = note.userInfo as? Dictionary<String, NSArray> {
                let jsonArray = info["jsonData"]
                self.seedTables(fromArray: jsonArray!)
            }
        }

    }
    
    lazy var context: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = self.createPersistentStoreCoordinator()
        return managedObjectContext
        } ()
        
    // Returns the URL to the application's Documents directory
    func applicationDocumentsDirectory() -> NSString {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! NSString
        if debug == true {
            print("documentsPath: \(documentsPath)")
        }
        return documentsPath
    }
    
    func createManagedObjectModel() -> NSManagedObjectModel {
        let modelURL = NSBundle.mainBundle().URLForResource("DiscountModel", withExtension: "momd")
        if let managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL!) {
            if debug == true {
                print("Successfully Creating Managed Object Model with \(modelURL!)")
            }
            return managedObjectModel
        }
        else {
            print("Managed Object Model failed")
            return NSManagedObjectModel()
        }
    }
    
    func createPersistentStoreCoordinator() -> NSPersistentStoreCoordinator {
        var persistentStoreCoordinator: NSPersistentStoreCoordinator
        var managedObjectModel: NSManagedObjectModel = createManagedObjectModel()
        var docsDir = applicationDocumentsDirectory()
        
        var storeURL = NSURL(fileURLWithPath: applicationDocumentsDirectory() as String)
        //var storeURL: NSURL = NSURL(fileURLWithPath: applicationDocumentsDirectory() as String)!
        storeURL = storeURL.URLByAppendingPathComponent("Discounts.sqlite")
        //var error: NSError?
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        do {
            try persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
            if debug == true {
                print("Persistent Store created successfully at \(storeURL)")
            }
        } catch let error as NSError{
            print("Unresolved error: \(error), \(error.userInfo)")
        }
        
        return persistentStoreCoordinator
    }
    
}

    

