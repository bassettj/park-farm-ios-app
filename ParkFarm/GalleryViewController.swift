//
//  GalleryViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 06/04/2016.
//  Copyright © 2016 Jeremy Bassett. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var textView: UITextView!
    
    var pageType: PageType?
    var galleryItem: Gallery!
    
    var images: [String] = []
    var imageCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryItem = Gallery(pageType: pageType!)
        images = galleryItem.images()
        textView.text = galleryItem.pageText()
        self.title = galleryItem.pageTitle()
    }
    
    func configureScrollView() {
        let viewWidth = self.view.frame.size.width
        print("View width: \(viewWidth)")
        
        let compensatedHeight = (viewWidth / 320.0 ) * 180.0
        
        imageCount = images.count
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0.0, y: 0.0, width: viewWidth, height: compensatedHeight)
        
        for index in 0 ..< imageCount {
            
            let xOrigin: CGFloat = CGFloat(index) * scrollView.frame.size.width
            let image = UIImage(named: images[index])
            let imageView = UIImageView(image: image!)
            
            imageView.frame = CGRect(x: xOrigin, y: 0.0, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
            print("Image Frame: \(imageView.frame)")
            scrollView.addSubview(imageView)
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(imageCount) * viewWidth, height: compensatedHeight)
        print("Frame: \(scrollView.frame.size)")
        print("Content: \(scrollView.contentSize)")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureScrollView()
        pageControl.numberOfPages = imageCount

    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        if scrollView == scrollView {
            pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width);
        }
        
    }


    @IBOutlet var scrollView: UIScrollView! {
        didSet {
            /*
            imageCount = images.count
            scrollView.pagingEnabled = true
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            scrollView.delegate = self
            scrollView.contentSize = CGSize(width: CGFloat(imageCount) * 320.0, height: 180.0)

            for index in 0 ..< imageCount {
                let xOrigin: CGFloat = CGFloat(index) * 320.0
                let image = UIImage(named: images[index])
                let imageView = UIImageView(image: image!)
                imageView.frame = CGRect(x: xOrigin, y: 0.0, width: 320.0, height: 180.0)
                print("Image Frame: \(imageView.frame)")
                scrollView.addSubview(imageView)
            }
            print("Frame: \(scrollView.frame.size)")
            print("Content: \(scrollView.contentSize)")
            */
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //scrollView.frame = CGRect(x: 0.0, y: 0.0, width: 320.0, height: 180.0)
        //scrollView.contentSize = CGSize(width: CGFloat(imageCount) * 320.0, height: 180.0)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
