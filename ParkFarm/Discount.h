//
//  Discount.h
//  ParkFarm
//
//  Created by Jeremy Bassett on 23/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DiscountGroup;

@interface Discount : NSManagedObject

@property (nonatomic, retain) NSString * company;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSNumber * orderId;
@property (nonatomic, retain) DiscountGroup *parentGroup;

@end
