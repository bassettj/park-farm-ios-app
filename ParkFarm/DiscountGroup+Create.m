//
//  DiscountGroup+Create.m
//  ParkFarm
//
//  Created by Jeremy Bassett on 07/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import "DiscountGroup+Create.h"

@implementation DiscountGroup (Create)

+ (DiscountGroup *)groupWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context {
    DiscountGroup *discountGroup = nil;
    
    if ([name length]) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"DiscountGroup"];
        request.predicate = [NSPredicate predicateWithFormat:@"groupName = %@", name];
        
        NSError *error;
        NSArray *matches = [context executeFetchRequest:request error:&error];
        
        if (!matches || [matches count] > 1) {
            // handle error
        }
        // we don't have one
        else if (!matches.count) {
            discountGroup = [NSEntityDescription insertNewObjectForEntityForName:@"DiscountGroup" inManagedObjectContext:context];
            discountGroup.name = name;
        }
        // else we have one
        else {
            discountGroup = [matches firstObject];
        }
    }
    
    return discountGroup;
}


@end
