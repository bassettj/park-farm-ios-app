//
//  PFUserRegistration.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 29/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

class PFUserRegistration : NSObject {
    
    var name: String?
    var email: String?
    
    init(name: String, email: String) {
        self.name = name
        self.email = email
    }
    
    func registerWithWebService() {
        /* TODO
        print("Sending \(name!) and \(email!) to web service...")
        let _name = name!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())
        let _email = email!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())
        
        let urlString = "\(WebServiceInfo.RegistrationURL)?name=\(_name!)&email=\(_email!)"
        print("Requesting with URL: \(urlString)");
        
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!)
        let myQueue = NSOperationQueue()
        NSURLConnection.sendAsynchronousRequest(request, queue: myQueue) {(response, data, error) in
            if error != nil {
                print("Error: \(error!.localizedDescription)")
                var alert = UIAlertView(title: "Park Farm", message: "Registration Failed", delegate: self, cancelButtonTitle: "Error: \(error!.localizedDescription)")
                alert.show()
            }
            else {
                print(NSString(data: data!, encoding: NSUTF8StringEncoding))
                var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary!
                print("JSON: \(jsonResult)")
                if let requestResponse = jsonResult["response"] as? NSString {
                    print("requestResponse: \(requestResponse)")
                    if requestResponse == "OK" {
                        print("OK")
                        var alert = UIAlertView(title: "Park Farm", message: "Registration Complete", delegate: self, cancelButtonTitle: "Dismiss")
                        alert.show()
                    }
                }
            }
        }
        */
    }

}
