{
"discountData": [
{
    "section": "Main Discounts",
    "rows": [
    {   "title": "50% off Dinner for Two",
        "subtitle": "Pizza Express",
        "url": "www.staylocal.com/pizzaexpress.php?appId=ParkFarm",
        "imageUrl": ""},
    {   "title": "Lunchtime Special",
        "subtitle": "Jamie Oliver's",
        "url": "www.staylocal.com/nandos?appIdPark=Farm",
        "imageUrl": ""},
    {   "title": "Free Entry for Kids",
        "subtitle": "Norwich City Ground Tour",
        "url": "www.staylocal.com/norwichcity.php?appId=ParkFarm",
        "imageUrl": ""}
    ]
},
{
    "section": "Latest Discounts",
    "rows": [
    {   "title": "Lunch Menu £6.95",
        "subtitle": "Chiquito's",
        "url": "www.staylocal.com/chiquito?appId=ParkFarm",
        "imageUrl": ""},
    {   "title": "2nd Pizza Free",
        "subtitle": "Pizza Hut",
        "url": "www.staylocal.com/pizzaexpress.php?appId=ParkFarm",
        "imageUrl": ""}
    ]
}
]
}