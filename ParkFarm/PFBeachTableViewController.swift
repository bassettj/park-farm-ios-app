//
//  PFBeachTableViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 28/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFBeachTableViewController: UITableViewController {
    
    class Beach {
        var title: String?
        var info: String?
        var url: String?
        var imageUrl: String?
        
        init(title: String, info: String, imageUrl: String) {
            self.title = title
            self.info = info
            self.imageUrl = imageUrl
        }
        
        func addBeach(title: String, info: String, url: String) {
            let newBeach = Beach(title: title, info: info, imageUrl: url)
        }
    }
    
    var beaches = [Beach]?()

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.style = .Grouped
        beaches = [Beach]()
        beaches?.append(Beach(title: "Newquay", info: "Party Central", imageUrl: "https://www.visitcornwall.com/sites/default/files/styles/product_image/public/Towan%20Beach%20-%20Newquay%20C_0.jpg?itok=eDmB6Az3"))
        beaches?.append(Beach(title: "St Ives", info: "Great food, great light", imageUrl: "http://www.cornwall-online.co.uk/penwith/images-stives/stives-13576604-2.jpg"))
        beaches?.append(Beach(title: "Padstow", info: "Famous Harbour Town ", imageUrl: "http://www.treverbynhouse.com/img/Bed-Breakfast-Padstow-beachA.jpg"))

    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destViewController = segue.destinationViewController as? PFBeachViewController {
            if let indexPath = tableView.indexPathForSelectedRow {
                let beach = beaches![indexPath.row] as Beach
                destViewController.beach = beach
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return beaches!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Beach Cell", forIndexPath: indexPath) 

        let beach = beaches![indexPath.row]
        cell.textLabel?.text = beach.title
        cell.detailTextLabel?.text = beach.info

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
