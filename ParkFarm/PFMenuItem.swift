//
//  PFMenuItem.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 28/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

class PFMenuItem {
    
    var pagetitle: String?
    var pdfFile: String?
    var pdfURL: String?
    var pdfFilePath: String? {
        get {
            let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
            let pdfFilePath = documentsPath.stringByAppendingPathComponent(pdfFile!)
            return pdfFilePath
        }
    }
    
    init(pagetitle: String, pdfFile: String, pdfURL: String) {
        self.pagetitle = pagetitle
        self.pdfFile = pdfFile
        self.pdfURL = pdfURL
        
        let fileManager = NSFileManager.defaultManager()
        let documentsPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let pdfFilePath = documentsPath.stringByAppendingPathComponent(pdfFile)
        let fileExists = fileManager.fileExistsAtPath(pdfFilePath)
        if fileExists {
            print("File \(pdfFile) exists in \(documentsPath)")
        }
        else {
            /* TODO
            let pdf = NSBundle.mainBundle().bundlePath
            let pdfUrl = NSURL(string: pdf)
            let pdfFilePath = pdfUrl?.string
            
            let pdfFilePathBundle = NSBundle.mainBundle().bundlePath.stringByAppendingPathComponent(self.pdfFile!)
            fileManager.copyItemAtPath(pdfFilePathBundle, toPath: pdfFilePath, error: nil)
            */
        }
        print("Setting pdfFile")

    }
    
}
