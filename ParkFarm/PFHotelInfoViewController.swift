//
//  PFHotelInfoViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 10/04/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFHotelInfoViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var pageScrollView: UIScrollView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet weak var infoLabel: UILabel!
    
    var scrollImages = ["Hotel Scroll Image 1", "Hotel Scroll Image 2", "Hotel Scroll Image 3", "Hotel Scroll Image 4"]

    var menus = [PFMenuItem]?()
    
    var imageCount = 0
    
    func configureScrollView() {
        let viewWidth = self.view.frame.size.width
        print("View width: \(viewWidth)")
        
        let compensatedHeight = (viewWidth / 320.0 ) * 180.0
        
        imageCount = scrollImages.count
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0.0, y: 0.0, width: viewWidth, height: compensatedHeight)
        
        for index in 0 ..< imageCount {
            
            let xOrigin: CGFloat = CGFloat(index) * scrollView.frame.size.width
            let image = UIImage(named: scrollImages[index])
            let imageView = UIImageView(image: image!)
            
            imageView.frame = CGRect(x: xOrigin, y: 0.0, width: scrollView.frame.size.width, height: scrollView.frame.size.height)
            print("Image Frame: \(imageView.frame)")
            scrollView.addSubview(imageView)
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(imageCount) * viewWidth, height: compensatedHeight)
        print("Frame: \(scrollView.frame.size)")
        print("Content: \(scrollView.contentSize)")
        

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        pageScrollView.contentSize = CGSize(width: self.view.bounds.width, height: self.view.bounds.height)
        pageScrollView.contentSize = CGSize(width: self.view.bounds.width, height: 1200.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageCount = scrollImages.count
        self.title = "Park Farm Hotel"
        infoLabel!.text = "Park Farm Country Hotel & Leisure\nHethersett\nNorwich\nNorfolk\nNR9 3DL"
        
        menus?.append(PFMenuItem(pagetitle: "Breakfast Menu", pdfFile: "Bar-Lunch-Menu.pdf", pdfURL: "\(WebServiceInfo.PdfLocation))/breakfast-menu.pdf"))
        menus?.append(PFMenuItem(pagetitle: "Lunch Menu", pdfFile: "Bar-Lunch-Menu.pdf", pdfURL: ""))
        menus?.append(PFMenuItem(pagetitle: "Room Service", pdfFile: "Bar-Lunch-Menu.pdf", pdfURL: ""))
        
        pageScrollView.showsHorizontalScrollIndicator = false
        pageScrollView.showsVerticalScrollIndicator = false
        //pageScrollView.contentSize = CGSize(width: 320.0, height: 180.0)
        
        pageControl.numberOfPages = imageCount
        
        configureScrollView()
        /*
        scrollView.delegate = self
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        for index in 0 ..< imageCount {
            let xOrigin: CGFloat = CGFloat(index) * 320.0
            let imageName = "Hotel Scroll Image \(index + 1)"
            let image = UIImage(named: scrollImages[index])
            let imageView = UIImageView(image: image!)
            //imageView.contentMode = .ScaleAspectFit
            //imageView.sizeToFit()
            let offsetOrigin: CGPoint = CGPointMake(CGFloat(index) * 320.0, 0.0)
            //imageView.frame.origin = offsetOrigin
            imageView.frame = CGRect(x: xOrigin, y: 0.0, width: 320.0, height: 180.0)
            scrollView.addSubview(imageView)
        }
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
        */
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            print("Identifier: \(identifier)")
            if let destinationViewController = segue.destinationViewController as? PFWebViewController {
                switch identifier {
                case SegueNames.ShowWeather:
                    print("segue = \(SegueNames.ShowWeather)")
                    destinationViewController.websiteAddress = "http://www.bbc.co.uk/weather/2641181"
                    destinationViewController.title = "Local Weather"
                case SegueNames.ShowClimate:
                    print("segue = \(SegueNames.ShowClimate)")
                    destinationViewController.websiteAddress = "http://en.wikipedia.org/wiki/Norwich#Climate"
                    destinationViewController.title = "Local Climate"
                case SegueNames.HotelWebsite:
                    print("segue = \(SegueNames.HotelWebsite)")
                    destinationViewController.websiteAddress = "http://www.parkfarm-hotel.co.uk"
                    destinationViewController.title = "Park Farm Website"
                case "Book a Stay":
                    destinationViewController.websiteAddress = "https://www.parkfarm-hotel.co.uk/book-online/?alNights=1&alStayOccs=1%2C0%2C0"
                    destinationViewController.title = "Book a Stay"
                case "Latest News":
                    destinationViewController.websiteAddress = "https://www.parkfarm-hotel.co.uk/about-us/news/"
                    destinationViewController.title = "Latest News"
                default: break
                }
            }
            else if let destinationVC = segue.destinationViewController as? GalleryViewController {
                print("Setting outlet for identifier: \(identifier)")
                switch identifier {
                case "Conferences":
                    destinationVC.pageType = .Conferences
                case "Weddings":
                    destinationVC.pageType = .Weddings
                case "Occasions":
                    destinationVC.pageType = .Occasions
                default:
                    break
                }
            }
            else if let destinationViewController = segue.destinationViewController as? PFDiscountsViewController {
                switch identifier {
                case SegueNames.ShowDiscounts:
                    destinationViewController.title = "Local Discounts"
                default: break
                }
            }
            else if let destinationViewController = segue.destinationViewController as? PFConciergeViewController {
                switch identifier {
                case SegueNames.ShowDiscounts:
                    destinationViewController.title = "Concierge Services"
                default: break
                }
            }
            else if let destinationViewController = segue.destinationViewController as? JBPDFViewController {
                switch identifier {
                case SegueNames.ShowBreakfastMenu:
                    let menuItem = menus![0] as PFMenuItem
                    print("menuItem = \(menuItem)")
                    destinationViewController.menu = menuItem
                case SegueNames.ShowLunchMenu:
                    let menuItem = menus![1] as PFMenuItem
                    print("menuItem = \(menuItem)")
                    destinationViewController.menu = menuItem
                case SegueNames.ShowRoomServiceMenu:
                    let menuItem = menus![2] as PFMenuItem
                    print("menuItem = \(menuItem)")
                    destinationViewController.menu = menuItem
                default: break
                }
            }
        }
    }
    /*
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("Scrollview height: \(scrollView.frame.size.height)")
        print("Scrollview width: \(scrollView.frame.size.width)")
        scrollView.contentSize = CGSize(width: CGFloat(imageCount) * self.view.frame.size.width, height: scrollView.frame.size.height)
    }
*/

    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        if scrollView == scrollView {
            pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width);
        }
        
    }
    

}
