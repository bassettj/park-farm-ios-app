//
//  DiscountGroup.m
//  ParkFarm
//
//  Created by Jeremy Bassett on 23/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import "DiscountGroup.h"
#import "Discount.h"


@implementation DiscountGroup

@dynamic name;
@dynamic discounts;

@end
