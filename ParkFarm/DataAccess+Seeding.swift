//
//  DataAccess+Seeding.swift
//  FetchResultsViewController
//
//  Created by Jeremy Bassett on 13/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

extension DataAccess {
        
    // MARK: Seeding Methods
    func seedTables(fromArray discountArray: NSArray) {
        
        // remove all data first
        removeAllDiscounts()
        removeAllDiscountGroups()
        
        DiscountGroup.CreateDiscountGroupsFromArray(array: discountArray, inManagedObjectContext: context)
        /*
        for bundleItem in discountArray {
            let newDiscountGroup = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", inManagedObjectContext: context) as DiscountGroup
            if let sectionString: AnyObject? = bundleItem["section"] {
                var section = sectionString! as String
                if debug == true {
                    print("Section Title = \(section)")
                }
                newDiscountGroup.name = section
            }
            let discountSet = NSMutableSet()
            if let rowArr: AnyObject? = bundleItem["rows"] {
                var rows = rowArr! as NSArray
                if debug == true {
                    print("Row Array: \(rows)")
                }
                for rowItem in rows {
                    let newDiscount = NSEntityDescription.insertNewObjectForEntityForName("Discount", inManagedObjectContext: context) as Discount
                    if debug == true {
                        print("Row Item = \(rowItem)")
                    }
                    if let title = rowItem["title"]! as? NSString {
                        newDiscount.title = title
                    }
                    if let subtitle = rowItem["subtitle"]! as? NSString {
                        newDiscount.subtitle = subtitle
                    }
                    if let url = rowItem["url"]! as? NSString {
                        newDiscount.url = url
                    }
                    discountSet.addObject(newDiscount)
                }
            }
            if debug == true {
                print("Discount Set = \(discountSet)")
            }
            newDiscountGroup.addDiscounts(discountSet)
            context.save(nil)
        }
        */
    }
    
    // MARK: Utility Methods
    func removeAllDiscountGroups() {
        print("Removing all Discount Group Data")
        let discGroupFetchRequest = NSFetchRequest(entityName: "DiscountGroup")
        
        let alldiscGroups: [DiscountGroup]
        
        do {
             alldiscGroups = try context.executeFetchRequest(discGroupFetchRequest) as! [DiscountGroup]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return
        }
        
        for dg in alldiscGroups {
            context.deleteObject(dg)
        }
        
        do {
            try context.save()
        } catch {
            // Handle Error
        }

        //try context.save()
        
        //let alldiscGroups = context.executeFetchRequest(discGroupFetchRequest, error: nil) as! [DiscountGroup]
    }
    
    func removeAllDiscounts() {
        print("Removing all Discount Data")
        let discGroupFetchRequest = NSFetchRequest(entityName: "Discount")
        
        let discountsForDeletion: [Discount]
        
        do {
            discountsForDeletion = try context.executeFetchRequest(discGroupFetchRequest) as! [Discount]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return
        }
        
        for doomedDiscount in discountsForDeletion {
            context.deleteObject(doomedDiscount)
        }
        
        do {
            try context.save()
        } catch {
            // Handle Error
        }
    }
    
    func printAllDiscountGroups() {
        let discFetchRequest = NSFetchRequest(entityName: "DiscountGroup")
        let primarySortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let allDiscounts: [DiscountGroup]
        
        do {
            allDiscounts = try context.executeFetchRequest(discFetchRequest) as! [DiscountGroup]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return
        }
        
        print("----- Discount Groups -----\n")
        for discount in allDiscounts {
            print("Discount Title: \(discount.name)\n")
        }
        print("---------------------------\n")
    }
    
    func printAllDiscounts() {
        let discFetchRequest = NSFetchRequest(entityName: "Discount")
        let primarySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        discFetchRequest.sortDescriptors = [primarySortDescriptor]
        
        let allDiscounts: [Discount]
        
        do {
            allDiscounts = try context.executeFetchRequest(discFetchRequest) as! [Discount]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
            return
        }
        
        print("-------- Discounts --------\n")
        for discount in allDiscounts {
            print("Group: \(discount.parentGroup.name); Discount Title: \(discount.name); Discount Subtitle: \(discount.info)\n")
        }
        print("---------------------------\n")
    }
    
    
    // MARK: Old Seeding Methods
    func seedTables() {
        
        removeAllDiscounts()
        removeAllDiscountGroups()
        
        let group = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", inManagedObjectContext: context) as! DiscountGroup
        group.name = "Main Discounts"
        //group.look()
        
        let d1 = NSEntityDescription.insertNewObjectForEntityForName("Discount", inManagedObjectContext: context) as! Discount
        d1.name = "Discount 1"
        
        let d2 = NSEntityDescription.insertNewObjectForEntityForName("Discount", inManagedObjectContext: context) as! Discount
        d2.name = "Discount 2"
        
        group.discounts = NSSet(objects: d1, d2) as! Set<Discount>;

        do {
            try context.save()
        } catch {
            // Handle Error
        }

        
        printAllDiscountGroups()
        printAllDiscounts()
    }
    
    func seedDiscounts() {
        
        // Grab Discounts
        let discGroupFetchRequest = NSFetchRequest(entityName: "Discount")
        
        let seedingDiscounts: [Discount]
        
        do {
            seedingDiscounts = try context.executeFetchRequest(discGroupFetchRequest) as! [Discount]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
        
        // Create an array of tuples, assigning
        let discounts = [
            (title: "Disc 1", subtitle: "Excellent Discount 1", url: "www.staylocal.co.uk", group: "Main Discounts"),
            (title: "Disc 2", subtitle: "Excellent Discount 2", url: "www.staylocal.co.uk", group: "Main Discounts"),
            (title: "Disc 3", subtitle: "Excellent Discount 3", url: "www.staylocal.co.uk", group: "Special Discounts"),
            (title: "Disc 4", subtitle: "Excellent Discount 4", url: "www.staylocal.co.uk", group: "Special Discounts"),
        ]
        
        for discount in discounts {
            let newDiscount = NSEntityDescription.insertNewObjectForEntityForName("Discount", inManagedObjectContext: context) as! Discount
            newDiscount.name = discount.title
            newDiscount.info = discount.subtitle
            newDiscount.url = discount.url
            /*
            let newDiscountGroup = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", inManagedObjectContext: context) as DiscountGroup
            newDiscountGroup.name = discount.group
            newDiscount.parentGroup = newDiscountGroup
            */
        }
        print("Seeding Complete")

        do {
            try context.save()
        } catch {
            // Handle Error
        }

    }
    
    func seedDiscountGroups() {
        
        // Grab Discounts
        let discGroupFetchRequest = NSFetchRequest(entityName: "DiscountGroup")
        
        let seedingDiscountGroups: [DiscountGroup]
        
        do {
            seedingDiscountGroups = try context.executeFetchRequest(discGroupFetchRequest) as! [DiscountGroup]
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
                
        // Create an array of tuples, assigning
        let discountGroups = [
            (title: "Disc 1", name: "Main Discounts"),
            (title: "Disc 1", name: "Special Discounts"),
        ]
        
        for group in discountGroups {
            let newDiscountGroup = NSEntityDescription.insertNewObjectForEntityForName("DiscountGroup", inManagedObjectContext: context) as! DiscountGroup
            newDiscountGroup.name = group.name
        }
        print("Seeding Complete")

        do {
            try context.save()
        } catch {
            // Handle Error
        }

    }
    
    struct Offer {
        var name: String
        var detail: String
        var url: String
    }
    
    struct OfferCollection {
        var offerGroup: String
        var offer: Array<Offer>
    }
    
    struct offerBundle {
        var group: Array<OfferCollection>
    }
    

}
