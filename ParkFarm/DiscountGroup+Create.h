//
//  DiscountGroup+Create.h
//  ParkFarm
//
//  Created by Jeremy Bassett on 07/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import "DiscountGroup.h"

@interface DiscountGroup (Create)

+ (DiscountGroup *)groupWithName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context;

@end
