//
//  Discount+WebService.m
//  ParkFarm
//
//  Created by Jeremy Bassett on 07/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import "Discount+WebService.h"
#import "DiscountGroup+Create.h"

@implementation Discount (WebService)

+ (Discount *)discountWithWebServiceInfo:(NSDictionary *)discountDict inManagedObjectContext:(NSManagedObjectContext *)context {
    Discount *discount = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Discount"];
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    NSLog(@"Number of matches: %lu", (unsigned long)[matches count]);
    if (error) {
        NSLog(@"Error: %@", error.userInfo);
    }
    
    if (!matches || error || [matches count] > 1) {
        // deal with error
        NSLog(@"Error: %@", error.userInfo);
    }
    else if ([matches count]) {
        discount = [matches firstObject];
    }
    else {
        
        NSLog(@"Inserting Discount Object...");
        
        discount.name = discountDict[@"discTitle"];
        discount.info = discountDict[@"discSubtitle"];
        discount.url = discountDict[@"discUrl"];
        discount.longitude = [NSNumber numberWithChar:(char)discountDict[@"longitude"]];
        discount.latitude = [NSNumber numberWithChar:(char)discountDict[@"latitude"]];
        discount = [NSEntityDescription insertNewObjectForEntityForName:@"Discount" inManagedObjectContext:context];
        
        NSString *groupName = [discountDict valueForKeyPath:@"groupName"];
        discount.parentGroup = [DiscountGroup groupWithName:groupName inManagedObjectContext:context];
    }
    
    return discount;
}

+ (void)loadDiscountsFromWebServiceArray:(NSArray *)discounts intoManagedObjectContext:(NSManagedObjectContext *)context {
    for (NSDictionary *discountItem in discounts) {
        NSString *discGroup = [discountItem valueForKeyPath:@"section"];
        NSArray *discArr = [discountItem valueForKeyPath:@"rows"];
        for (NSDictionary *rowDict in discArr) {
            NSString *discTitle = [rowDict valueForKeyPath:@"title"];
            NSString *discSubtitle = [rowDict valueForKeyPath:@"subtitle"];
            NSString *discUrl = [rowDict valueForKeyPath:@"url"];
            NSDictionary *senderDict = @{@"groupName": discGroup, @"discTitle": discTitle, @"discSubtitle": discSubtitle, @"discUrl": discUrl};
            [Discount discountWithWebServiceInfo:senderDict inManagedObjectContext:context];
        }
    }
}


@end
