//
//  Discount+WebService.h
//  ParkFarm
//
//  Created by Jeremy Bassett on 07/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

#import "Discount.h"

@interface Discount (WebService)

+ (Discount *)discountWithWebServiceInfo:(NSDictionary *)discountDict inManagedObjectContext:(NSManagedObjectContext *)context;
+ (void)loadDiscountsFromWebServiceArray:(NSArray *)discounts intoManagedObjectContext:(NSManagedObjectContext *)context;


@end
