//
//  PFRoomService.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 01/04/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

class PFRoomService {
    
    var itemGroups: [ServiceItemGroup]!
    var guestSurname: String?
    var guestEMail: String?
    var guestPhoneNumber: String?
    
    init(itemGroups: [ServiceItemGroup]) {
        
        let firstItem = ServiceItem(item: "Champagne", info: "Bubbles", price: 23.99)
        let secondItem = ServiceItem(item: "Hamper", info: "Lovely Food", price: 34.99)
        
        let firstGroup = ServiceItemGroup(title: "On Arrival", items: [firstItem, secondItem])
        
        let thirdItem = ServiceItem(item: "Flowers", info: "12 Roses", price: 12.99)
        let fourthItem = ServiceItem(item: "Wine", info: "Rioja", price: 17.99)
        
        let secondGroup = ServiceItemGroup(title: "Pamper Items", items: [thirdItem, fourthItem])
        
        self.itemGroups = [firstGroup, secondGroup]
        
    }
    
    func seeding() -> PFRoomService {
        let firstItem = ServiceItem(item: "Champagne", info: "Bubbles", price: 23.99)
        let secondItem = ServiceItem(item: "Hamper", info: "Lovely Food", price: 34.99)
        
        let firstGroup = ServiceItemGroup(title: "Pamper Items", items: [firstItem, secondItem])
        
        let thirdItem = ServiceItem(item: "Flowers", info: "12 Roses", price: 12.99)
        let fourthItem = ServiceItem(item: "Wine", info: "Rioja", price: 17.99)
        
        let secondGroup = ServiceItemGroup(title: "Pamper Items", items: [thirdItem, fourthItem])
        let groups = PFRoomService()
        
        groups.itemGroups = [firstGroup, secondGroup]
        
        return groups
        
    }
    
    init() {
        
    }
    
    init(seeding: Bool) {
        if seeding == true {
            
            guestSurname = "Bassett"
            guestEMail = "jer.bassett@googlemail.com"
            guestPhoneNumber = "07793 411 914"
            
            let firstItem = ServiceItem(item: "Champagne", info: "Bubbles", price: 23.99)
            let secondItem = ServiceItem(item: "Hamper", info: "Lovely Food", price: 34.99)
            
            let firstGroup = ServiceItemGroup(title: "Pamper Items", items: [firstItem, secondItem])
                        
            let thirdItem = ServiceItem(item: "Flowers", info: "12 Roses", price: 12.99)
            let fourthItem = ServiceItem(item: "Wine", info: "Rioja", price: 17.99)
            
            let secondGroup = ServiceItemGroup(title: "Pamper Items", items: [thirdItem, fourthItem])
            
            self.itemGroups = [firstGroup, secondGroup]
            
        }
    }
    
    func urlEncodedString(str: String) -> String? {
        let customAllowedSet =  NSCharacterSet.URLQueryAllowedCharacterSet()
        let escapedString = str.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
        return escapedString
    }
    
    func apiCall() -> String {
        let urlString = constructUrl()
        let urlStr = urlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        let url = NSURL(string: urlStr!)
        let request = NSURLRequest(URL: url!)
        var queue: NSOperationQueue = NSOperationQueue()
        
        
        let downloadTask = NSURLSession.sharedSession().dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                self.handleReponseData(data!)
            }
        })
        downloadTask.resume()
        
        
//        NSURLConnection.sendAsynchronousRequest(request, queue: queue) { (response: NSURLResponse!, responseData: NSData!, error: NSError!) -> Void in
//            if error != nil {
//                NSNotificationCenter.defaultCenter().postNotificationName(Notification.ConciergeOrderNotification, object: nil, userInfo: ["status": "Connection", "errorMessage": error.localizedDescription])
//            }
//            else {
//                self.handleReponseData(responseData)
//            }
//        }
        return urlString
    }
    
    private func handleReponseData(responseData: NSData) {
        do {
            let jsonResult = try NSJSONSerialization.JSONObjectWithData(responseData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
            print("JSON [Concierge]: \(jsonResult)")
            if let statusResponse = jsonResult["response"] as? NSString {
                print("statusResponse: \(statusResponse)")
                if statusResponse == "OK" {
                    NSNotificationCenter.defaultCenter().postNotificationName(Notification.ConciergeOrderNotification, object: nil, userInfo: ["status": statusResponse])
                }
                else {
                    if let errorMessage = jsonResult["errorDescription"] as? NSString {
                        NSNotificationCenter.defaultCenter().postNotificationName(Notification.ConciergeOrderNotification, object: nil, userInfo: ["status": statusResponse, "errorMessage": errorMessage])
                    }
                }
            }
        } catch let error as NSError {
            print("Error: \(error.localizedDescription)")
        }
    }
    
    
    private func constructUrl() -> String {
        var urlPath: String = WebServiceInfo.ConciergeProduction
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let testAPIRequested = userDefaults.boolForKey("enable_test")
        
        if testAPIRequested {
            print("Test API Requested")
            if let secretWord = userDefaults.stringForKey("passcode") {
                if secretWord == WebServiceInfo.APIKey {
                    print("Secret Word Match")
                    urlPath = WebServiceInfo.ConciergeTest
                }
                else {
                    print("Secret Word Missmatch \(secretWord) not equal \(WebServiceInfo.APIKey)")
                }
            }
        }
        
        let groups = itemGroups as [ServiceItemGroup]
        
        urlPath = urlPath + "?surname=" + urlEncodedString(guestSurname!)! + "&email=" + urlEncodedString(guestEMail!)! + "&telno=" + urlEncodedString(guestPhoneNumber!)! + "&order="
        
        for itemGroup in groups {
            let groupItems = itemGroup.items! as [ServiceItem]
            for item in groupItems {
                if item.selected == true {
                    urlPath = urlPath + urlEncodedString(item.item!)! + "\n"
                }
            }
        }
        return urlPath
    }
    
    func orderValue() -> String {
        var totalOrderValue = 0.00
        let groups = itemGroups as [ServiceItemGroup]
        
        for itemGroup in groups {
            let groupItems = itemGroup.items! as [ServiceItem]
            for item in groupItems {
                if item.selected == true {
                    totalOrderValue = totalOrderValue + item.price!
                }
            }
        }
        if totalOrderValue == 0.0 {
            return "No Items Selected"
        }
        else {
            return "Total Order Amount: £\(totalOrderValue)"
        }
    }
    
    func clearOrder() {
        let groups = itemGroups as [ServiceItemGroup]
        
        for itemGroup in groups {
            let groupItems = itemGroup.items! as [ServiceItem]
            for item in groupItems {
                item.selected = false
                print("Resetting")
            }
        }
    }
    
    func cellTitleForIndexPath(indexPath: NSIndexPath) -> String {
        if indexPath.section > 0 {
            let group = itemGroups[indexPath.section - 1] as ServiceItemGroup
            let items = group.items!
            let good = items[indexPath.row]
            return good.item!
        }
        else {
            return "Guest Details"
        }
    }
    
    func markItemForIndexPath(indexPath: NSIndexPath, selected: Bool) {
        if indexPath.section > 0 {
            let group = itemGroups[indexPath.section - 1] as ServiceItemGroup
            let items = group.items!
            let good = items[indexPath.row]
            good.selected = selected
        }
    }
    
    func itemIsSelectedForIndexPath(indexPath: NSIndexPath) -> Bool {
        if indexPath.section > 0 {
            let group = itemGroups[indexPath.section - 1] as ServiceItemGroup
            let items = group.items!
            let good = items[indexPath.row]
            return good.selected!
        }
        else {
            return false
        }
    }
    
    func detailsAreComplete() -> Bool {
        if guestSurname != "" && guestEMail != "" && guestPhoneNumber != "" {
            return true
        }
        else {
            return false
        }
    }
    
    func cellSubtitleTitleForIndexPath(indexPath: NSIndexPath) -> String {
        if indexPath.section > 0 {
            let group = itemGroups[indexPath.section - 1] as ServiceItemGroup
            let items = group.items!
            let good = items[indexPath.row]
            return "\(good.info!): £\(good.price!)"
        }
        else {
            if detailsAreComplete() == true {
                return "Complete"
            }
            else {
                return "Incomplete"
            }
        }
    }
    
    func numberOfSections() -> Int {
        return itemGroups.count + 1
    }
    
    func numberOfCellForSection(section: Int) -> Int {
        if section > 0 {
            let group = itemGroups[section - 1]
            let item = group.items
            return group.items!.count
        }
        else {
            return 1
        }
    }
    
    func sectionTitleForIndexPath(section: Int) -> String {
        if section > 0 {
            let group = itemGroups[section - 1]
            return group.title!
        }
        else {
            return "Guest Details"
        }
    }
    
    class ServiceItemGroup {
        
        var title: String?
        var items: [ServiceItem]?
        
        init(title: String, items: [ServiceItem]) {
            self.title = title
            self.items = items
        }
        
    }
    
    class ServiceItem {
        
        var item: String?
        var info: String?
        var price: Double?
        var selected: Bool?
        
        init(item: String, info: String, price: Double) {
            self.item = item
            self.info = info
            self.price = price
            self.selected = false
        }
        
    }
    
    
}