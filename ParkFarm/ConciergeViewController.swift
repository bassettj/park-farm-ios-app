//
//  ConciergeViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 19/04/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class ConciergeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var placeOrderButton: UIBarButtonItem!
    var roomService: PFRoomService!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        roomService = PFRoomService(seeding: true)
        self.title = "Concierge"
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: Selector("orderRequested:"))
        NSNotificationCenter.defaultCenter().addObserverForName(Notification.ConciergeOrderNotification, object: nil, queue: NSOperationQueue.mainQueue()) { (note) -> Void in
            if let userInfo = note.userInfo {
                print("userInfo: \(userInfo)")
                if let status = userInfo["status"] as? String {
                    if status == "OK" {
                        let alert = UIAlertView(title: "Park Farm", message: "Order Successful", delegate: self, cancelButtonTitle: "Dismiss")
                        alert.show()
                        self.roomService.clearOrder()
                        self.tableView.reloadData()
                    }
                    else {
                        if let errorDescription = userInfo["errorMessage"] as? String {
                            var alert = UIAlertView(title: "Park Farm", message: "Order failed.\n\(errorDescription).\nPlease try again later.", delegate: self, cancelButtonTitle: "Dismiss")
                            alert.show()
                        }
                    }
                }
            }
        }
        super.viewDidLoad()
    }
    
    func clearOrder() {
        roomService.clearOrder()
        tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: Notification.ConciergeOrderNotification, object: nil)
        super.viewWillDisappear(animated)
    }
    
    func orderRequested(sender: UIBarButtonItem) {
        let actionSheet = UIActionSheet(title: "Concierge", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Place Order")
        actionSheet.showInView(view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 1 {
            let url = roomService.apiCall()
            print("URL: \(url)");
        }
    }
    
    @IBAction func placeOrder(sender: UIBarButtonItem) {
        let url = roomService.apiCall()
        print("URL: \(url)");
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Service Cell", forIndexPath: indexPath) as! UITableViewCell
        
        cell.textLabel?.text = roomService.cellTitleForIndexPath(indexPath)
        cell.detailTextLabel?.text = roomService.cellSubtitleTitleForIndexPath(indexPath)
        
        if indexPath.section == 0 {
            cell.accessoryType = .DisclosureIndicator
        }
        else {
            if roomService.itemIsSelectedForIndexPath(indexPath) == true {
                cell.accessoryType = .Checkmark;
            }
            else {
                cell.accessoryType = .None;
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 0 {
            return "\(roomService.orderValue())"
        }
        else {
            return ""
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let destinationViewController = segue.destinationViewController as? GuestDetailsTableViewController {
            destinationViewController.roomService = roomService
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            performSegueWithIdentifier("Show Guest Details", sender: nil)
        }
        else {
            roomService.markItemForIndexPath(indexPath, selected: !roomService.itemIsSelectedForIndexPath(indexPath))
            print("Order value: \(roomService.orderValue())")
            tableView.reloadData()
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return roomService.sectionTitleForIndexPath(section)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return roomService.numberOfSections()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomService.numberOfCellForSection(section)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
