//
//  constants.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 07/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

let DiscountDatabaseAvailablilityNotication: String = "DiscountDatabaseAvailablilityNotication"
let DiscountDatabaseAvailablilityContext: String = "Context"

struct Notification {
    static let CoreDataUpdatedNotification = "CoreDataUpdatedNotification"
    static let WebServiceUpdatedNotification = "WebServiceUpdatedNotification"
    static let ConciergeOrderNotification = "ConciergeOrderNotification"
    static let RefreshButtonPressed = "RefreshButtonPressed"
}

struct WebServiceInfo {
    static let Production = "http://www.pocketlogic.co.uk/parkfarm/offers.php"
    static let Test = "http://oceanremix-development.co.uk/offers.php"
    static let ConciergeProduction = "http://www.pocketlogic.co.uk/parkfarm/handleConciergeOrder.php"
    static let ConciergeTest = "http://oceanremix-development.co.uk/handleConciergeOrder.php"
    static let RegistrationProduction = "http://www.pocketlogic.co.uk/parkfarm/parkfarm-register.php"
    static let RegistrationTest = "http://oceanremix-development.co.uk/parkfarm-register.php"
    static let RegistrationURL = "http://www.pocketlogic.co.uk/parkfarm-register.php"
    static let PdfLocation = "http://www.pocketlogic.co.uk/parkfarm/pdfs"
    static let APIKey = "0ceanR3mix"
}

struct SegueNames {
    static let ShowBreakfastMenu = "Show Breakfast Menu"
    static let ShowLunchMenu = "Show Lunch Menu"
    static let ShowRoomServiceMenu = "Show Room Service Menu"
    static let HotelWebsite = "Show Hotel Website"
    static let ShowClimate = "Show Climate"
    static let ShowWeather = "Show Weather"
    static let ShowDiscounts = "Show Discounts"
    static let ShowDiscountDetail = "Show Discount Detail"
    static let ShowRegistration = "Show Registration"
}
