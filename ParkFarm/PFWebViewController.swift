//
//  PFWebViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 03/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//


import UIKit

class PFWebViewController: UIViewController, UIWebViewDelegate {
    
    var websiteAddress: String?

    @IBOutlet var webView: UIWebView! {
        didSet {
            print("didSet WebView: \(websiteAddress!)")
            webView.delegate = self
            updateUI()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        updateUI()
        super.viewWillAppear(animated)
        print("viewWillAppear: \(websiteAddress!)")
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad: \(websiteAddress)")
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("Finished Loading")
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        print("\(error?.localizedDescription)")
    }
    
    func updateUI() {
        print("website = \(websiteAddress!)")
        if let webAddress = websiteAddress {
            if let url = NSURL(string: webAddress) {
                let request = NSURLRequest(URL: url)
                print("Request: \(request)")
                self.webView.loadRequest(request)
            }
        }
        else {
            print("No go")
        }
    }

}