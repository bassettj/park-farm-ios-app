//
//  WebService.swift
//  FetchResultsViewController
//
//  Created by Jeremy Bassett on 15/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

class WebService: NSObject, NSURLConnectionDelegate {
    
    let test = false
    let debug = false
    /*
    init() {
        print("Setting up Web Service");
        NSNotificationCenter.defaultCenter().addObserverForName("Refresh Now", object: nil, queue: nil) { (note) -> Void in
            print("Notification Received")
            self.fetchDiscountData()
        } 
    }
    */
    func fetchDiscountData() {
        print("Fetching Data....");
        if test == true {
            jsonFromFile()
        }
        else {
            performRequest()
        }
    }
    
    func jsonFromFile() -> NSArray {
        var newdata: NSDictionary = NSDictionary()
        var discountArray: NSArray = NSArray()
        if let path = NSBundle.mainBundle().pathForResource("parkfarm", ofType: "php") {
            
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    if let arrayOfDiscounts = jsonResult["offers"] as? [NSArray] {
                        return arrayOfDiscounts
                    }
                } catch {
                    print("ERROR: Read from file failed")
                    return NSArray()
                }
            } catch {
                print("ERROR: File does not exist")
                return NSArray()
            }

            
            
            
            
//            if let jsonData = NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingUncached, error: nil) {
//                //print("Raw Json Data: \(jsonData)")
//                var error: NSErrorPointer = nil;
//                
//                newdata = NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableLeaves, error: nil) as! NSDictionary
//                //print("newdata: \(newdata)")
//                discountArray = newdata["offers"]! as! NSArray
//            } else {
//                print("ERROR: Read from file failed")
//            }
        } else {
            print("ERROR: File does not exist")
        }
        //print("discountArray: \(discountArray)")
        let userInfo = ["jsonData": discountArray]
        NSNotificationCenter.defaultCenter().postNotificationName(Notification.CoreDataUpdatedNotification, object: nil, userInfo: userInfo)
        return discountArray
    }
    
    let responseData = NSMutableData()
    
    func performRequest() {
        var urlPath: String = WebServiceInfo.Production
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        let testAPIRequested = userDefaults.boolForKey("enable_test")
    
        if testAPIRequested {
            print("Test API Requested")
            if let secretWord = userDefaults.stringForKey("passcode") {
                if secretWord == WebServiceInfo.APIKey {
                    print("Secret Word Match")
                    urlPath = WebServiceInfo.Test
                }
                else {
                    print("Secret Word Missmatch \(secretWord) not equal \(WebServiceInfo.APIKey)")
                }
            }
        }
        
        print("Fetching Data from \(urlPath)....");
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connection.start()
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.responseData.appendData(data)
    }
    
    typealias APICallback = ((AnyObject?, NSError?) -> ())
    var callback: APICallback! = nil
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        print("connectionDidFinishLoading called")
        
        
        do {
            let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
        } catch {
            print("ERROR: Read from file failed")
            //callback(nil, error)
            return
        }
        
        
//        var error: NSError?
//        var json : AnyObject! = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.MutableLeaves, error: &error)
//        if error != nil {
//            callback(nil, error)
//            return
//        }
        
        // TODO
        //handleReturnedJSON(jsonResult)
/*
        switch(statusCode, self.path!) {
        case (200, Path.SIGIN_IN):
            callback(self.handleSignIn(json), nil)
        default:
            // Unknown Error
            callback(nil, nil)
        }
*/
    }
    
    func handleReturnedJSON(json: AnyObject) {
        if debug == true {
            print("Web Data (JSON): \(json)")
        }
        let discountTimestamp = json["timestamp"]! as! String
        print("discountTimestamp = \(discountTimestamp)")
        
        var requiredUpdate = true
        var serverTimestamp: NSDate?
        
        let dateStringFormatter = NSDateFormatter()
        dateStringFormatter.dateFormat = "dd/MM/yyyy"
        //dateStringFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        if let serverTimestamp = dateStringFormatter.dateFromString(discountTimestamp) as NSDate! {
            print("timestamp = \(serverTimestamp)")
            if let coreDataTimestamp = NSUserDefaults.standardUserDefaults().objectForKey("coreDataTimestamp") as? NSDate {
                switch coreDataTimestamp.compare(serverTimestamp) {
                case NSComparisonResult.OrderedDescending:
                    print("Requires Update")
                    requiredUpdate = false
                default:
                    break
                }
            }
        }
        
        if requiredUpdate {
            let discountArray = json["offers"]! as! NSArray
            let userInfo = ["jsonData": discountArray]
            NSNotificationCenter.defaultCenter().postNotificationName(Notification.WebServiceUpdatedNotification, object: nil, userInfo: userInfo)
            //NSUserDefaults.standardUserDefaults().setObject(serverTimestamp!, forKey: "coreDataTimestamp")
        }

    }

    
    func jsonFromWebService() -> NSArray {
        var newdata: NSDictionary = NSDictionary()
        var discountArray: NSArray = NSArray()
        if let path = NSBundle.mainBundle().pathForResource("parkfarm", ofType: "php") {
            
            
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    if let arrayOfDiscounts = jsonResult["offers"] as? [NSArray] {
                        return arrayOfDiscounts
                    }
                } catch {
                    print("ERROR: Read from file failed")
                    return NSArray()
                }
            } catch {
                print("ERROR: File does not exist")
                return NSArray()
            }
            
            
            
            
            
            
//            if let jsonData = NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingUncached, error: nil) {
//                //print("Raw Json Data: \(jsonData)")
//                var error: NSErrorPointer = nil;
//                
//                newdata = NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableLeaves, error: nil) as! NSDictionary
//                //print("newdata: \(newdata)")
//                discountArray = newdata["offers"]! as! NSArray
//            } else {
//                print("ERROR: Read from file failed")
//            }
        } else {
            print("ERROR: File does not exist")
        }
        //print("discountArray: \(discountArray)")
        let userInfo = ["jsonData": discountArray]
        //NSNotificationCenter.defaultCenter().postNotificationName(Notification.WebServiceUpdatedNotification, object: nil, userInfo: userInfo)
        return discountArray
    }
    

}
