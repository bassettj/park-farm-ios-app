//
//  PFInfoViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 05/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFInfoViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet var pageControl: UIPageControl!
    
    var images = [String]?()
    var registration: PFUserRegistration!
    let imageCount = 4
    
    @IBAction func cancelRegistration(segue:UIStoryboardSegue) {
        
    }
    
    @IBAction func registerUserDetails(segue: UIStoryboardSegue) {
        print("Unwind Segue running = \(segue.sourceViewController)")
        
        var srcViewController = segue.sourceViewController as? UIViewController
        print("Unwind Segue running = \(srcViewController)")
        if let navCon = srcViewController! as? UINavigationController {
            srcViewController = navCon.visibleViewController
            print("Extracting from Navigation Controller...")
        }
        if let registrationViewController = srcViewController as? PFRegistrationViewController {
            print("Registration View Controller found....")
            let userRegistration = registrationViewController.registration
            print("userRegistration = \(userRegistration.name)")
            userRegistration.registerWithWebService()
        }
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("Preparing for Segue...")
        /*
        if segue.identifier == SegueNames.ShowRegistration {
            print("Preparing for PFRegistrationViewController...")
            
            var destViewController = segue.destinationViewController as? UIViewController
            // this next if-statement makes sure the segue prepares properly even - if the MVC we're seguing to is wrapped in a UINavigationController
            if let navCon = destViewController as? UINavigationController {
                destViewController = navCon.topViewController
                print("Extracting from Navigation Controller...")
            }
            if let registrationViewController = destViewController as? PFRegistrationViewController {
                print("Preparing for PFUserRegistration...")
                registration = PFUserRegistration(name: "Jeremy Bassett", email: "jer.bassett@googlemail.com")
                registrationViewController.registration = registration
            }
        }
        */
    }

    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x / scrollView.frame.size.width);
        
        print("Scrollview height: \(scrollView.frame.size.height)")
        print("Scrollview width: \(scrollView.frame.size.width)")

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("Scrollview height: \(scrollView.frame.size.height)")
        print("Scrollview width: \(scrollView.frame.size.width)")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("Scrollview height: \(scrollView.frame.size.height)")
        print("Scrollview width: \(scrollView.frame.size.width)")
        scrollView.contentSize = CGSize(width: CGFloat(imageCount) * self.view.frame.size.width, height: scrollView.frame.size.height)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoLabel!.text = "Park Farm Country Hotel & Leisure\nHethersett\nNorwich\nNorfolk\nNR9 3DL"
        pageControl.numberOfPages = imageCount
        scrollView.delegate = self
        scrollView.pagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        print("Scrollview height: \(scrollView.frame.size.height)")
        print("Scrollview width: \(scrollView.frame.size.width)")
        
        //scrollView.contentSize = CGSize(width: CGFloat(imageCount) * self.view.frame.size.width, height: 180.0)
        
        for var index = 0; index < imageCount; index++ {
            let xOrigin: CGFloat = CGFloat(index) * self.view.frame.size.width
            let imageName = "Hotel Scroll Image \(index + 1)"
            let image = UIImage(named: imageName)
            //print(imageName)
            //let image = UIImage(image: "Hotel Scroll Image /(index)")
            let imageView = UIImageView(image: image!)
            //imageView.frame = CGRect(x: xOrigin, y: 0.0, width: 320.0, height: 180.0)
            imageView.frame = CGRect(x: xOrigin, y: 0.0, width: image!.size.width, height: image!.size.height)
            scrollView.addSubview(imageView)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
