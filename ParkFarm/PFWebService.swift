//
//  PFWebService.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 06/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFWebService: NSObject, NSURLConnectionDelegate {
    
    lazy var data = NSMutableData()
        
    func connect() {
        //data = NSMutableData()
        
        let urlPath: String = WebServiceInfo.Production
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: true)!
        connection.start()
        
        //print("Web Service Init")
        
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        print("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection!, didReceiveData conData: NSData!) {
        self.data.appendData(conData)
    }
    
    var newdata: NSDictionary = NSDictionary()
    
    func jsonFromFile() -> NSArray {
        // TODO
        if let path = NSBundle.mainBundle().pathForResource("parkfarm-test", ofType: "php") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSDictionary = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    if let arrayOfDiscounts = jsonResult["offers"] as? [NSArray] {
                        return arrayOfDiscounts
                    }
                } catch {
                    print("ERROR: Read from file failed")
                    return NSArray()
                }
            } catch {
                print("ERROR: File does not exist")
                return NSArray()
            }
        }
        else {
            return NSArray()
        }
        
        return NSArray()
        
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        
        print("connectionDidFinishLoading")
        //print(data)
        //var stringArray : Array = []<String>
        let dataStr = NSString(data: data, encoding: NSASCIIStringEncoding)
        print(dataStr)
        
        //var err: NSError
        // throwing an error on the line below (can't figure out where the error message is)
        // TODO
        //var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary!
        /*
        var error: NSErrorPointer = nil
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: error) as NSDictionary
        */
        //print(jsonResult)
        
        
        
        /*
        var dersler : NSArray = jsonResult.valueForKey("tbl_aboutus") as NSArray
        print(dersler)
        for vartype : AnyObject in dersler
        {
            var dersler1 : NSString = vartype.valueForKey("abtus_desc") as NSString
            stringArray.addObject(dersler1)
        }
        print(stringArray)
        */
    }
    
    
   
}
