//
//  PFBeachViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 28/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFBeachViewController: UIViewController {

    var beach: PFBeachTableViewController.Beach?
    
    @IBOutlet var maintitle: UILabel!
    @IBOutlet var subtitle: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        
        self.title = beach!.title
        maintitle.text = beach!.info
        //subtitle.text = beach!.info
        
        if let urlString = beach!.imageUrl {
            print(urlString)
            JBImageLoader.sharedLoader.imageForUrl(urlString, completionHandler:{(image: UIImage?, url: String) in
                self.imageView.image = image
            })
            /*
            let url = NSURL(string: urlString)
            let imageData = NSData(contentsOfURL: url!)
            imageView.image = UIImage(data: imageData!)
            */
        }
        super.viewDidLoad()
    }

}
