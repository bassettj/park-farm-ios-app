//
//  ArticleViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 06/04/2016.
//  Copyright © 2016 Jeremy Bassett. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet var headline: UILabel!
    @IBOutlet var subheader: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var descriptionTextView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Occasions"
        headline.text = "Occasions"
        
        subheader.text = ""
        descriptionTextView.text = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
