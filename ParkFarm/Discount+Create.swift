//
//  Discount+Create.swift
//  FetchResultsViewController
//
//  Created by Jeremy Bassett on 14/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import Foundation

extension Discount {
    
    class func DiscountWithDictionary(dictionary dict: NSDictionary, inManagedObjectContext context: NSManagedObjectContext) -> Discount {
        print("Adding new discount...")
        var newDiscount = Discount()
        if let title = dict["title"]! as? NSString {
            newDiscount.name = title as String
        }
        if let subtitle = dict["subtitle"]! as? NSString {
            newDiscount.info = subtitle as String
        }
        if let url = dict["url"]! as? NSString {
            newDiscount.url = url as String
        }
        if let latitude = dict["latitude"]! as? NSString {
            print("Lat: \(latitude)")
            //newDiscount.latitude = latitude.
        }
        return newDiscount
    }
    
    class func DiscountSetFromArray(array: NSArray, inManagedObjectContext context: NSManagedObjectContext) -> NSSet {
        var discountSet = NSMutableSet()
        
        for dict in array as! [NSDictionary] {
            discountSet.addObject(Discount.DiscountWithDictionary(dictionary: dict, inManagedObjectContext: context))
        }
        
        return discountSet as NSSet
    }
    
    func show() -> String? {
        return "name: \(name), company: \(company), info: \(info), url: \(url), parent: \(parentGroup)"
    }
    /*
    func annotationForOffer() -> OfferAnnotation {
        
        let annotation = OfferAnnotation(coordinate: CLLocationCoordinate2DMake(longitude.doubleValue, latitude.doubleValue), title: name, subtitle: info)
        return annotation
    }
*/
}