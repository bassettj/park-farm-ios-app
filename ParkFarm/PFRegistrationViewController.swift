//
//  PFRegistrationViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 29/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class PFRegistrationViewController: UITableViewController {
    
    var registration: PFUserRegistration!
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        registration.name = nameTextField.text
        registration.email = emailTextField.text
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registration = PFUserRegistration(name: "", email: "")
        nameTextField.isFirstResponder()
    }

}
