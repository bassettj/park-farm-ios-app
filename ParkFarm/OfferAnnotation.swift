//
//  OfferAnnotation.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 29/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit
import MapKit

class OfferAnnotation: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
    
    /*
    convenience init(title: String, subtitle: String, longitude: Double, latitude: Double) {
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        //init(coordinate: c, title: title, subtitle: subtitle)
        super.init(coordinate: )
    }
*/

}
