//
//  JBPDFViewController.swift
//  ParkFarm
//
//  Created by Jeremy Bassett on 26/03/2015.
//  Copyright (c) 2015 Jeremy Bassett. All rights reserved.
//

import UIKit

class JBPDFViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView : UIWebView!
    
    var menu: PFMenuItem?
    
    var pdfURLString: String! {
        didSet {
            
            let pdfFileComponents = pdfURLString!.componentsSeparatedByString(".") as [String]
            let pdfFileComponent = pdfFileComponents.first
            let pdfFileExtension = pdfFileComponents.last
            
            print("pdfFileComponent = \(pdfFileComponent) pdfFileExtension = \(pdfFileExtension)")

            let bundlePath = NSBundle.mainBundle().URLForResource(pdfFileComponent!, withExtension: pdfFileExtension!)
            let request = NSURLRequest(URL: bundlePath!)
            webView.loadRequest(request)
        
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //view.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
        if let menuItem = menu {
            //pdfURLString = menuItem.pdfFile!
            let url = NSURL(fileURLWithPath: menuItem.pdfFilePath!)
            let request = NSURLRequest(URL: url)
            webView.loadRequest(request)

            self.title = menuItem.pagetitle!
        }
        //pdfURLString = menu!.pdfFile!
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        print("Loaded \(webView)")
    }
    
    
}